# HELMES City Task



## Getting started

How to get the application running. There are two ways you can do that or one. I don't know.
1. If i managed to set up Docker then all you have to do is run
`docker-compose up` to start the application and 'docker-compose down' to stop it. Ofcourse make sure you have Docker on your machine :)
2. Now if docker doesn't work, you should :
* First create a MySQL database named `city_list` .
* Then open `city-list` folder and run the `CityListApplication.java` file to start up the back end.
* Then open `city-list-front-end` folder and in a new Terminal window type `npm start`.
* Now you should be able to use the application on `http://localhost:3000/`

