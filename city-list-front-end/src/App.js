import "./App.css";
import NavBarComponent from "./components/NavBar/NavBarComponent";
import HomePage from "./Pages/HomePage";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <NavBarComponent input={"test"} />
      <HomePage />
    </div>
  );
}

export default App;
