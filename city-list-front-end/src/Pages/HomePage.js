import { Container } from "react-bootstrap";
import FetchCities from "../components/FetchCities";

function HomePage() {
  return (
    <Container fluid>
      <FetchCities />
    </Container>
  );
}

export default HomePage;
