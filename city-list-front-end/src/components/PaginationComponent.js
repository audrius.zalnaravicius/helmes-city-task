import { Component } from "react";
import Pagination from "react-js-pagination";

class PaginationComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 15,
    };
  }

  render() {
    return (
      <Pagination
        hideNavigation
        activePage={this.props.activePage}
        itemsCountPerPage={this.props.itemsCountPerPage}
        totalItemsCount={this.props.totalItemsCount}
        pageRangeDisplayed={10}
        itemClass="page-item"
        linkClass="btn btn-light"
        onChange={this.props.onChange}
      />
    );
  }
}

export default PaginationComponent;
