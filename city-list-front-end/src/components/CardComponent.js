import { Component } from "react";
import { Button } from "react-bootstrap";
import ModalComponent from "./ModalComponent";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";

class CardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: "photo.png",
    };
  }

  render() {
    return (
      <Col key={this.props.article.id}>
        <Card style={{ height: "20rem" }}>
          <Card.Img
            variant="top"
            src={this.props.article.photo}
            style={{ maxHeight: "10rem" }}
          />
          <Card.Body className="row align-items-center">
            <Card.Title>{this.props.article.name}</Card.Title>
            <Button
              variant="primary"
              onClick={() => this.setState({ modalShow: true })}
            >
              Edit
            </Button>
          </Card.Body>
          <ModalComponent
            show={this.state.modalShow}
            onHide={() => this.setState({ modalShow: false })}
            cityName={this.props.article.name}
            cityPhoto={this.props.article.photo}
            cityId={this.props.article.id}
          />
        </Card>
      </Col>
    );
  }
}

export default CardComponent;
