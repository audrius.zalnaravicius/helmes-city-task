import React from "react";
import Row from "react-bootstrap/Row";
import { Container } from "react-bootstrap";
import PaginationComponent from "./PaginationComponent";
import Form from "react-bootstrap/Form";
import CityDataService from "../services/city.service";
import CardComponent from "./CardComponent";
import { searchURL, fetchURL } from "../global/Variables";

class FetchCities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      articlesDetails: [],
      activePage: 0,
      totalPages: null,
      itemsCountPerPage: null,
      totalItemsCount: 0,
      modalShow: false,
      inputText: "",
    };
    this.handlePageChange = this.handlePageChange.bind(this);
    this.fetchURL = this.fetchURL.bind(this);
  }

  inputHandler = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    this.setState({ inputText: lowerCase });
    lowerCase !== ""
      ? this.fetchURL(this.state.activePage, searchURL + lowerCase)
      : this.fetchURL(this.state.activePage, fetchURL);
  };

  fetchURL(page, url) {
    CityDataService.getCities(page, url).then((response) => {
      const totalPages = response.data.totalPages;
      const itemsCountPerPage = response.data.size;
      const totalItemsCount = response.data.totalElements;
      this.setState({ totalPages: totalPages });
      this.setState({ totalItemsCount: totalItemsCount });
      this.setState({ itemsCountPerPage: itemsCountPerPage });

      const results = response.data.content;

      const updatedResults = results.map((results) => {
        return {
          ...results,
        };
      });

      this.setState({ articlesDetails: updatedResults });
    });
  }

  componentDidMount() {
    this.fetchURL(this.state.activePage, fetchURL);
  }

  handlePageChange(pageNumber) {
    var pageNum = pageNumber - 1; //-1 because Pagable starts from index 0
    this.setState({ activePage: pageNum });
    this.fetchURL(pageNum, fetchURL);
  }

  populateRowsWithData = () => {
    const articleData = this.state.articlesDetails.map((article) => {
      return <CardComponent article={article} />;
    });

    return articleData;
  };

  render() {
    return (
      <Container>
        <Form.Group className="mb-5 mt-5">
          <Form.Control
            type="search"
            placeholder="Search cities"
            className="me-2 mb-2"
            aria-label="Search"
            onChange={this.inputHandler}
          />
        </Form.Group>
        <Row xs={1} md={5} className="g-4">
          {this.populateRowsWithData()}
        </Row>
        <Form className="d-flex"></Form>
        <div className="d-flex justify-content-center mt-3">
          <PaginationComponent
            activePage={this.state.activePage}
            itemsCountPerPage={this.state.itemsCountPerPage}
            totalItemsCount={this.state.totalItemsCount}
            onChange={this.handlePageChange}
          />
        </div>
      </Container>
    );
  }
}

export default FetchCities;
