import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import Form from "react-bootstrap/Form";
import CityDataService from "../services/city.service";
import { editURL } from "../global/Variables";

function ModalComponent(props) {
  const [state, setState] = useState({
    id: props.cityId,
    name: props.cityName,
    photo: props.cityPhoto,
    visible: false,
  });

  function handleChange(evt) {
    const value = evt.target.value;

    setState({
      ...state,
      [evt.target.name]: value,
    });
    console.log("VALUEE " + state.name);
  }

  const handleCityEdit = () => {
    CityDataService.editCity(editURL, props.cityId, state).then(() => {
      window.location.reload(false);
    });
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Edit</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Enter new city Name and Photo URL</h4>
        <InputGroup className="mb-3">
          <InputGroup.Text id="inputGroup-sizing-default">
            City Name
          </InputGroup.Text>
          <Form.Control
            aria-label="City Name"
            aria-describedby="inputGroup-sizing-default"
            placeholder={props.cityName}
            name="name"
            onChange={handleChange}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroup.Text id="inputGroup-sizing-default">
            City Photo
          </InputGroup.Text>
          <Form.Control
            aria-label="City Photo"
            aria-describedby="inputGroup-sizing-default"
            placeholder={props.cityPhoto}
            name="photo"
            onChange={handleChange}
          />
        </InputGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={handleCityEdit}>
          Submit
        </Button>
        <Button variant="danger" onClick={props.onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
export default ModalComponent;
