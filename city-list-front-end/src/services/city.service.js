import axios from "axios";

const CityDataService = {
  getCities(page, url) {
    return axios({
      method: "GET",
      url: `${url}?page=${page}&size=10`,
      headers: {
        Authorization: "Basic ",
      },
    });
  },
  editCity(url, id, data) {
    return axios({
      method: "PUT",
      url: `${url}${id}`,
      headers: {
        Authorization: "Basic ",
      },
      data: data,
    });
  },
};

export default CityDataService;
