package com.telesoftas.citylist.controller;

import com.telesoftas.citylist.model.City;
import com.telesoftas.citylist.repository.CityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CityControllerTests {
    private final CityRepository cityRepository = mock(CityRepository.class);
    private final CityController cityController = new CityController(cityRepository);

    @Test
    void getAllCities() {
        Pageable pageable = PageRequest.of(0, 10);
        List<City> cityList = new ArrayList<>();
        cityList.add(new City());
        cityList.add(new City());
        Page<City> cities = new PageImpl<>(cityList);
        when(cityRepository.findAll(pageable)).thenReturn(cities);

        ResponseEntity<Page<City>> result = cityController.getAllCities(pageable);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(cities, result.getBody());
    }

    @Test
    void getAllCitiesByName() {
        Pageable pageable = PageRequest.of(0, 10);
        List<City> cityList = new ArrayList<>();
        cityList.add(new City());
        cityList.add(new City());
        Page<City> cities = new PageImpl<>(cityList);
        when(cityRepository.findAllByNameContains("name", pageable)).thenReturn(cities);

        Page<City> result = cityController.getAllCitiesByName("name", pageable);
        assertEquals(cities, result);
    }

    @Test
    void editCity() {
        City city = new City();
        city.setName("city");
        when(cityRepository.existsById(1L)).thenReturn(true);
        when(cityRepository.save(city)).thenReturn(city);

        ResponseEntity<City> result = cityController.editCity(1L, city);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(city, result.getBody());
    }

    @Test
    void editCity_CityNotFound() {
        City city = new City();
        city.setName("city");
        when(cityRepository.existsById(1L)).thenReturn(false);

        ResponseEntity<City> result = cityController.editCity(1L, city);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        assertFalse(result.hasBody());
    }
}

