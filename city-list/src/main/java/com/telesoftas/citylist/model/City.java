package com.telesoftas.citylist.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Setter
public class City {
    @Id @GeneratedValue
    private long id;
    @NonNull
    private String name;
    @NonNull
    @Column(length = 2000)
    private String photo;
}
