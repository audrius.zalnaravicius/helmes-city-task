package com.telesoftas.citylist.controller;

import com.telesoftas.citylist.model.City;
import com.telesoftas.citylist.repository.CityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/cities", produces = "application/json")
@CrossOrigin(origins = "*")
class CityController {

    private final CityRepository cityRepository;

    public CityController(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @GetMapping()
    public ResponseEntity<Page<City>> getAllCities(@PageableDefault Pageable pageable) {
        Page<City> cities = cityRepository.findAll(pageable);
        return cities.hasContent() ? ResponseEntity.ok(cities) : ResponseEntity.noContent().build();


    }

    @GetMapping("/search/{name}")
    public Page<City> getAllCitiesByName(@PathVariable String name, Pageable pageable) {
        Page<City> cities = cityRepository.findAllByNameContains(name, pageable);
        return cities;
    }


    @PutMapping("/{id}")
    public ResponseEntity<City> editCity(@PathVariable Long id, @RequestBody City city) {
        if (!cityRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        city.setId(id);
        City updatedCity = cityRepository.save(city);
        return ResponseEntity.ok(updatedCity);
    }

}
